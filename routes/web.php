<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RecommController@index');
Route::get('/recommend/create', 'RecommController@create');
Route::post('/recommend', 'RecommController@store');
Route::get('/recommend/{recommend}', 'RecommController@show'); //this uses a wild card for the ur
Route::get('/list', 'ListController@index');
Route::get('/tasks/create', 'TasksController@create'); //this will nav to create method
Route::get('/tasks/{task}', 'TasksController@show'); //this uses a wild card for the url
Route::post('/tasks', 'TasksController@store');
Route::post('/tasks/{task}/comments', 'CommentsController@store');
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');
Route::get('/logout', 'SessionsController@destroy');
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/movies', 'TasksController@movies');
Route::get('/friends/{friend}', 'RecommController@friend');
