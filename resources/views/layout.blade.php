<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Scripts -->
        <script
			  src="https://code.jquery.com/jquery-2.2.4.min.js"
			  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
              crossorigin="anonymous">
        </script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet"> 
        <!-- Stylesheets -->
        <link href="/css/app.css" rel="stylesheet">
   
    </head>
    <body>
        <div class="container">
             @include ('partials.flash')
            <section>
                @if (Auth::check())
                <aside>
                     @include ('partials.header')
                </aside>
                 @endif
                <article>
                @if (Auth::check())
                    @include ('partials.current')
                @endif
                    <div class="content">
                        @yield ('content')
                    </div>
                
                </article>
            </section>
        </div>
    </body>
</html>
