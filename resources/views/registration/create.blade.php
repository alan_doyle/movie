@extends ('layout')
@section ('content')
    <div id="outer" class="welcome">
        <div id="table-container">
            <div id="table-cell">
                <h1 class="m3">
                    Register Now
                </h1>
                <div class="actions">
                    <form method="POST" action="/register" class="small_form">
                        {{ csrf_field() }}
                        <label>Name</label>
                        <input type="text" id="name" name="name" required/>
                        <label>Email</label>
                        <input type="email" id="email" name="email" required />        
                        <label>Password</label>
                        <input type="password" id="password" name="password" required/> 
                        <label>Password Confirmation</label>
                        <input type="password" id="password_confirmation" name="password_confirmation" required/> 
                         @include ('partials.errors')
                        <button type="submit"> Register</button>
                    </form>
                    <p> Already a member? <a href="/login">Login now</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection