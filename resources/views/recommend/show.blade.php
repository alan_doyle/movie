@extends ('layout')
@section ('content')
 
 <div class="card mt-3">
  <div>
    <h1>{{ $recommend->title }}</h1>
    <h4>Recommended to {{ $recommend->email }} on {{ $recommend->created_at->toFormattedDateString() }}</h4>
    <p>{{ $recommend->body }}</p>
    
@foreach ($updates as $update)
    @if ($update->status == 'incomplete')
        <form action="/recommend/{{ $recommend->id }}" method="POST">
        {{ csrf_field() }}


        <button>Mark as watched</button>
        </form>
    @endif
@endforeach




  </div>
</div>

@endsection