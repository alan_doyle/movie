  @extends ('layout')
  @section ('content')
    {{--  @if (!is_array($recomms))
        <div class="actions">
            <h2 class="m3">
                Looks like you have no recommendations why not</br>
                recommend a movie to a friend first?
            </h2>
            <a href="/tasks/create" type="button" class="button">Lets do it</a>
        </div>
    @endif  --}}
   
        {{--  <ul>
            @foreach ($recomms as $recomm)
                <li>
                    <a href="/recommend/{{ $recomm->id }}">{{ $recomm->title }} - to {{ $recomm->email }}</a>
                </li>
            @endforeach
        </ul>  --}}


        <ul>
            @foreach ($lists as $list)
                <li>
                    <a href="/recommend/{{ $list->id }}">{{ $list->title }} - to {{ $list->email }}
                    @if ($list->status == 'complete')
                        <div style="height:10px;width:10px;background:green;border-radius:100%;float:right;margin-top:8px;"></div>
                    
                    @endif                   
                    </a>
                </li>
            @endforeach
        </ul>
  @endsection
  
 