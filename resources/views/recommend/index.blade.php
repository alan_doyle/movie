@extends ('layout')
@section ('content')

     @if (Auth::check())
        <ul>

{{--  @foreach ($recomms as $recomm)
  <li><a href="/friends/{{ $recomm->name }}">{{ $recomm->name }}</a></li>
 
@endforeach  --}}
            @foreach ($recomms as $recomm)
            @if (Auth::user()->name != $recomm->name)
                    <li>
                        <a href="/friends/{{ $recomm->name }}">
                                {{$recomm->name}}
                        </a>
                    </li>
            @endif
                
                        {{--  <a href="/recommend/{{ $recomm->id }}">
                            {{ $recomm->title }}, from 
                            @foreach ($users as $user)
                                @if ($user->id == $recomm->user_id)
                                    {{ $user->name}}
                                @endif
                            @endforeach 
                        </a>  --}}
            @endforeach 

        </ul>
    @endif
@endsection