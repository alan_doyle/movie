@extends ('layout')
@section ('content')
<ul>

    @foreach ($lists as $list)
@if (Auth::user()->email == $list->email)
    <li>
        <a href="/recommend/{{ $list->id }}">
                {{ $list->title }}
                @if ($list->status == 'complete')
                    <div style="height:10px;width:10px;background:green;border-radius:100%;float:right;margin-top:8px;"></div>
                
                @endif
           
        </a>
        
    </li>
     @endif
    @endforeach 

</ul>
@endsection
 
