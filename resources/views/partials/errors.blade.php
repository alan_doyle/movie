@if (count($errors))
    <ul class="error_list">
        @foreach ($errors->all() as $error)
        <li> {{ $error }} </li>
        @endforeach
    </ul>
@endif