@component('mail::message')
# New Recommendation

@foreach ($users as $user)
    @if ($user->id == $data->user_id)
    @endif
@endforeach

{{ $user->name}} has sent you a new recommendation. 



@component('mail::button', ['url' => 'http://34.250.1.72'])
View Recommendation
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
