@extends ('layout')
@section ('content')
    <div id="outer" class="welcome">
        <div id="table-container">
            <div id="table-cell">
                <h1 class="m3">
                    Login
                </h1>
                <div class="actions">
                    <form method="POST" action"/login" class="small_form">
                        {{ csrf_field() }}
                        <label>Email</label>
                        <input type="email" id="email" name="email" required />        
                        <label>Password</label>
                        <input type="password" id="password" name="password" required/> 
                        @include ('partials.errors')
                        <button type="submit"> Login</button>  
                    </form>  
                    <p> Not a member? <a href="/register">Register now</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection