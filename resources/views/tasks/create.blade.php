@extends ('layout')
@section ('content')
<h1>Recommend a movie</h1>
<div class="actions">
    <form method="POST" action="/tasks" class="small_form">
        {{ csrf_field() }}
        <div>
            <input type="text" placeholder="Movie title..." id="title" name="title" required/>
            <textarea placeholder="Say a few words about this movie.." id="body" name="body" required></textarea>
             @include ('partials.errors')
            <button type="submit"> Recommend movie </button>
        </div>
    </form>
    @include ('partials.select')
</div>
@endsection
