  @extends ('layout')
  @section ('content')
    {{--  @if (!is_array($recomms))
        <div class="actions">
            <h2 class="m3">
                Looks like you have no recommendations why not</br>
                recommend a movie to a friend first?
            </h2>
            <a href="/tasks/create" type="button" class="button">Lets do it</a>
        </div>
    @endif  --}}
    <div class="actions">
        @foreach ($recomms as $recomm)
            <li>
                <a href="/tasks/{{ $recomm->task->id }}">{{ $recomm->task->title }}</a> - from {{ $recomm->user->name }}<hr>
            </li>
        @endforeach
    </div>
  @endsection
  
 