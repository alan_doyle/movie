@extends ('layout')
@section ('content')
<div class="card mt-3">
  <div>
    <h5>{{ $task->title }}</h5>
    <h6>Created by {{ $task->user->name }} on {{ $task->created_at->toFormattedDateString() }}</h6>
    <p>{{ $task->body }}</p>
  </div>
</div>

    <ul>
        @foreach ($task->comments as $comment)
        <li>
            {{ $comment->body }} <hr>
        </li>
        @endforeach
    </ul>

    <form method="POST" action="/recommended/{{ $task->id }}">
        {{ csrf_field() }}
        <select name="recomm_id">
            @foreach ($users as $user)
                <option value="{{ $user->id }}">
                    {{ $user->name }} <hr>
                </option>
            @endforeach       
        </select>
        <button type="submit">Recommend</button>
    </form>




    <form method="POST" action="/tasks/{{ $task->id }}/comments">
        {{ csrf_field() }}
        <input type="text" placeholder="Write a comment..." title="body" name="body" required/>
        <button type="submit"> Comment </button>
    </form>
     @include ('partials.flash')
    @include ('partials.errors')
@endsection
