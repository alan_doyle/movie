@extends ('layout')
@section ('content')
    @if (Auth::check())
    <ul>
        <div>
            @foreach ($recomms as $recomm)
                @include ('tasks.task')
            @endforeach
        </div>
    </ul>
    @endif
    @if (! Auth::check())
        <div id="outer" class="welcome">
            <div id="table-container">
                <div id="table-cell">
                    <h1 class="m3">
                        The best way to recommend movies to </br>
                        friends, family and colleagues.
                    </h1>
                    <div class="actions">
                       <a class="secondary" href="/login">Log in</a>
                       <a class="button" href="/register">Sign up</a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

