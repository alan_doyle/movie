<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Recomm;
use App\User;


class Invite extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Recomm $data)
    {
        $this->data = $data;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $users = User::latest()->get(); 
        return $this->markdown('emails.invite', compact('users'));

    }
}
