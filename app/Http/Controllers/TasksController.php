<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task; //imports model
use App\User;
use App\Recomm;
class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {      
        $recomms =  Recomm::data()->get();
        return view('tasks.index', compact('recomms'));  
        // $tasks = Task::latest()->get(); //selects all tasks from database and lists in desc order
        // return view('tasks.index', compact('tasks')); // compacts db results and displays to view
    }

    public function show(Task $recommend) //accepts wild card from route
    {
        $users = User::latest()->get();
        return view('recommend.show', compact('recommend','users')); //displays the task in the view
    }

    public function create()
    {
        return view('tasks.create');
    }


    public function store() 

    {
        // create new task using request data
        // save it to db
        // redirect to page

        // $task = new Task;
        //$task->body = request('body');
        //$task->save();
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
        
        Task::create([
            'title' => request('title'),
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

        session()->flash('message', 'Successfull posted, thanks');


        return redirect('/'); 
    }
}
