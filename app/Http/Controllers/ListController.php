<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Recomm;

class ListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $current_user = auth()->id();
        $lists = DB::table('users')
                    ->join('recomms', 'recomms.user_id', '=', 'users.id')
                    ->join('actions', 'actions.recomm_id', '=', 'recomms.id')
                    ->where('users.id', '=', $current_user)
                    ->orderBy('recomms.created_at', 'desc')
                    ->get();

        $recomms = Recomm::data()->get();
        $link = Route::currentRouteName();
        return view('recommend.list', compact('recomms','link','lists'));     
    }
}
