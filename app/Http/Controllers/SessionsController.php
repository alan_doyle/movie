<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }


    public function create()
    {
        return view('sessions.create');
    }


    public function store()
    {
        // attempt to sign in the user and authenticate
        if (! Auth()->attempt(request(['email','password'])))
        {
            return back()->withErrors([
                'message' => 'Something is not working'
            ]);
        }

        return redirect('/');

    }

    public function destroy()
    {
        auth()->logout();
        return redirect('/');
    }
}
