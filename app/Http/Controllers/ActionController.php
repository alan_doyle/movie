<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Recomm;
use App\Task;
use App\Action;
use App\Mail\Invite;


class ActionController extends Controller
{
    public function index()
    {
         $actions =  Action::data()->get();
         return view('recommend.index', compact('actions')); 
    }

    public function update($id) //accepts wild card from route
    {
        $updates = DB::table('actions')
                    ->where('recomm_id', $id)
                    ->update(['status' => "complete"]);
 
        return redirect('/');
    }
}
