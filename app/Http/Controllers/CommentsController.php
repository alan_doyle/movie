<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;
use App\Comment;

class CommentsController extends Controller
{
    public function store(Task $task) //finds the task
    {
        $this->validate(request(),[
            'body' => 'required|min:2'
        ]);
        
        $task->addComment(request('body')); //runs the add comment class in Task model
        return back();
    }
}
