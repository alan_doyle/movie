<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Recomm;
use App\Task;
use App\Action;
use App\Mail\Invite;


class RecommController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }


    public function index()
    {   
        $recomms = DB::table('recomms')
                    ->join('actions', 'actions.recomm_id', '=', 'recomms.id')
                    ->join('users', 'recomms.user_id', '=', 'users.id')
                    ->whereColumn([
                        ['recomms.user_id', '=', 'users.id']
                    ])
                    // ->orWhere(function ($query) {
                    //     $query->where('recomms.user_id', '==', 'users.id')
                    //           ->where('actions.user_id', '==', 'users.email');
                    // })
                    ->select('name')
                    ->groupBy('name')
                    ->get();

        $names = DB::table('users')
                    ->join('recomms', 'recomms.user_id', '=', 'users.id')
                    ->where('recomms.user_id', '=', 'users.id')
                    ->select('name')
                    ->get();

        // $test = auth()->id();
        // $recomms = DB::table('users')
        //             ->join('recomms', 'recomms.user_id', '=', 'users.id')
        //             ->join('actions', 'actions.recomm_id', '=', 'recomms.id')
        //             // ->whereColumn([
        //             //     ['recomms.user_id', '=', 'users.id']
        //             // ])
        //             ->where('recomms.user_id', '=', 'users.id')              
        //             ->select('name')
        //             ->groupBy('users.name')
        //             ->get();                  


        $users = User::latest()->get(); 
        $link = Route::currentRouteName();
        return view('recommend.index', compact('recomms','link', 'users','names'));       

    }


    public function create()
    {
        $recomms = DB::table('users')
                    ->join('actions', 'actions.user_id', '=', 'users.email')
                    ->join('recomms', 'recomms.id', '=', 'actions.recomm_id')
                    ->whereColumn([
                        ['actions.user_id', '=', 'users.email']
                    ])
                    ->get();

        $link = Route::currentRouteName();
        return view('recommend.create', compact('recomms','link')); 
    }


    public function show(Recomm $recommend) //accepts wild card from route
    {
        $updates = DB::table('actions')
                    ->where('recomm_id', '=', $recommend->id)
                    ->select('status')
                    ->get();


        $actions = Action::latest()->get();
        $link = Route::currentRouteName();
        return view('recommend.show', compact('recommend','users', 'link','updates')); //displays the task in the view
    }

    public function friend($friend)
    {
        $lists = DB::table('users')
                    ->join('recomms', 'recomms.user_id', '=', 'users.id')
                    ->join('actions', 'actions.recomm_id', '=', 'recomms.id')
                    ->where('users.name', '=', $friend)
                    ->orderBy('recomms.created_at', 'desc')
                    ->get();


        $users = User::latest()->get();
        $link = Route::currentRouteName();
        return view('recommend.friend', compact('lists','users', 'link', 'statuses')); //displays the task in the view
    }


    public function store() //accepts body from the comment controller
    {

        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required',
            'email' => 'required|email',
        ]);


        if (! User::where('email', '=', Input::get('email'))->exists()) {

            $data = Recomm::create([
                'title' => request('title'),
                'body' => request('body'),
                'email' => request('email'),
                'user_id' => auth()->id()
            ]);


            $lastInsertedId = $data->id;
            $status = 'incomplete';

            $action = Action::create([
                'recomm_id' =>  $lastInsertedId,
                'user_id' => request('email'),
                'invite' => 1,
                'status' => $status
            ]);

            \Mail::to($data)->send(new Invite($data ));

            session()->flash('message', 'he has been invited');
        }

        elseif (User::where('email', '=', Input::get('email'))->exists()) {

            $data = Recomm::create([
                'title' => request('title'),
                'body' => request('body'),
                'email' => request('email'),
                'user_id' => auth()->id(),
            ]);

            $lastInsertedId = $data->id;

            $status = 'incomplete';

            $action = Action::create([
                'recomm_id' =>  $lastInsertedId,
                'user_id' => request('email'),
                'invite' => 1,
                'status' => $status
            ]);

            session()->flash('message', 'ecommend blood');

        }


        
        // session()->flash('message', 'Successfull recomm, thanks');

        return redirect('/'); 
    }




  


}
