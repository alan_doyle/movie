<?php

namespace App;


class Task extends Model

{
    public function scopeIncomplete($query)
    {
        return $query->where('completed', 0);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    // public function recomms()
    // {
    //     return $this->hasMany(Recomm::class);
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($body) //accepts body from the comment controller
    {
        Comment::create([ //creates new comment
            'body' => $body,
            'task_id' => $this->id
        ]);
    }
}
