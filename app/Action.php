<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [
        'recomm_id', 'user_id', 'invite', 'status'
    ];


    public function recomms()
    {
        return $this->belongsTo(Recomm::class);
    }

    public function scopeData($query)
    {
        $current_user = 'maria@example.com';
        return $query->where("user_id", "=", $current_user);
    }



}
