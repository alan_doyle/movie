<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recomm extends Model
{
    protected $fillable = [
        'user_id', 'title', 'body', 'email'
    ];


    // public function task()
    // {
    //     return $this->belongsTo(Task::class);
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    public function scopeData($query)
    {
        $current_user = auth()->id();
        return $query->where("user_id", "=", $current_user)->orderBy('created_at', 'desc');
    }


}
